import Vue from "vue";
// 高德地图
// 高德离线地图
import VueAMap from "vue-amap";
Vue.use(VueAMap);

VueAMap.initAMapApiLoader({
  // 高德key
  key: "72e462a2984d4db567f949275d0ac39d",
  plugin: [
    "AMap.Geocoder",
    "Autocomplete",
    "PlaceSearch",
    "Scale",
    "OverView",
    "ToolBar",
    "MapType",
    "PolyEditor",
    "AMap.CircleEditor",
    "AMap.Marker"
  ],
  // 默认高德 sdk 版本为 1.4.4
  v: "1.4.4",
});
